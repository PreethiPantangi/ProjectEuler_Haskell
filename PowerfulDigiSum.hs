import Data.Char

digiSum = sum . map digitToInt . show
problem_56 = maximum $ map digiSum [a^b | a <- [1..100], b <- [1..100]]

main = do
print $ problem_56
