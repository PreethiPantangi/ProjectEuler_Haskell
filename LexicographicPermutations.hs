import Data.List

sol = (!! 999999) . sort $ permutations ['0'..'9']

main = print $ sol
