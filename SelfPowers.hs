a = sum $ [x^x | x <- [1..1000]]
b = reverse $ take 10 $ reverse . show $ a

main = print $ b